## Sensor 90.295-F74, 232137000

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9f6d-fca5fbdfdd0a`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9f6d-fca5fbdfdd0a)
### UUID: `0184ebd9-988c-7bbb-9f6d-fca5fbdfdd0a`
### identifier: `fst-inv:T01`

</div>

## Keywords: Pt100, Temperatur

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Jumo |
| name: | 90.295-F74, 232137000 |
| serial number: | 97450008 |
| used procedure: | Pt100 |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-9f6d-fca5fbdfdd0a/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

