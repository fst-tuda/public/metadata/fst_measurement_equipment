## Sensor PAA-23/8465-10

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-baa7-03af1e126e9f`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-baa7-03af1e126e9f)
### UUID: `0184ebd9-988b-7bb9-baa7-03af1e126e9f`
### identifier: `fst-inv:D64`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-23/8465-10 |
| serial number: | 88624 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Normprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-baa7-03af1e126e9f/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

