## Sensor 1-C2/10kN

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-964d-711052667695`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-964d-711052667695)
### UUID: `0184ebd9-988c-7bbb-964d-711052667695`
### identifier: `fst-inv:K04`

</div>

## Keywords: DMS, Kraft

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | HBM |
| name: | 1-C2/10kN |
| serial number: | 1000/148893 |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-964d-711052667695/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

