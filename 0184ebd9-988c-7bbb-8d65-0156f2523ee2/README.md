## Sensor P9V

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8d65-0156f2523ee2`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8d65-0156f2523ee2)
### UUID: `0184ebd9-988c-7bbb-8d65-0156f2523ee2`
### identifier: `fst-inv:D115`

</div>

## Keywords: Druck, absolut

## General Info

| property | value |
|-:|:-|
| comment: | vor Gebrauch kalibrieren |
| manufacturer: | HBM |
| name: | P9V |
| serial number: | 370 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | None |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | Kabel und Netzteil |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-8d65-0156f2523ee2/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

