## Sensor PAA-33X/300bar

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-8002-afd53c8a4f26`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-8002-afd53c8a4f26)
### UUID: `0184ebd9-988b-7bba-8002-afd53c8a4f26`
### identifier: `fst-inv:D085`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-33X/300bar |
| serial number: | 1090057 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-8002-afd53c8a4f26/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

