## Sensor U10M/12,5kN

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9870-4fa72eb37059`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9870-4fa72eb37059)
### UUID: `0184ebd9-988c-7bbb-9870-4fa72eb37059`
### identifier: `fst-inv:K11`

</div>

## Keywords: DMS, Kraft

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | HBM |
| name: | U10M/12,5kN |
| serial number: | M12.51723354959n |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Prüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-9870-4fa72eb37059/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

