## Sensor PSI 9016

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b75c-8b63586cbeb5`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b75c-8b63586cbeb5)
### UUID: `0184ebd9-988b-7bb9-b75c-8b63586cbeb5`
### identifier: `fst-inv:D51`

</div>

## Keywords: Differenzdruck, Druck, relativ

## General Info

| property | value |
|-:|:-|
| comment: | 10 Kanäle, zusammen mit D50 |
| manufacturer: | Pressure Systems, Inc. |
| name: | PSI 9016 |
| serial number: | S/N 1648 |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Saul |
| last known location: | Ventilatorprüfstände |
| last modification: | 2014-08-21T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b75c-8b63586cbeb5/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

