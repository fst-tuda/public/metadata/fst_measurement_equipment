## Sensor KTE56200GQ4

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-81ff-5c01dcec45ba`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-81ff-5c01dcec45ba)
### UUID: `0184ebd9-988b-7bba-81ff-5c01dcec45ba`
### identifier: `fst-inv:D091`

</div>

## Keywords: Druck, Piezoresistiv, relativ

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Sensortechnics |
| name: | KTE56200GQ4 |
| serial number: | None |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-81ff-5c01dcec45ba/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

