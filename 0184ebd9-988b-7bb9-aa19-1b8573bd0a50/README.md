## Sensor PD-23/8666.1-0.2

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aa19-1b8573bd0a50`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aa19-1b8573bd0a50)
### UUID: `0184ebd9-988b-7bb9-aa19-1b8573bd0a50`
### identifier: `fst-inv:D001`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PD-23/8666.1-0.2 |
| serial number: | 83714 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-aa19-1b8573bd0a50/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

