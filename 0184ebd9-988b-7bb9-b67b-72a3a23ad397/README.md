## Sensor PU50_1

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b67b-72a3a23ad397`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b67b-72a3a23ad397)
### UUID: `0184ebd9-988b-7bb9-b67b-72a3a23ad397`
### identifier: `fst-inv:D47`

</div>

## Keywords: Druck

## General Info

| property | value |
|-:|:-|
| comment: | defekt |
| manufacturer: | Halstrup Multur |
| name: | PU50_1 |
| serial number: | 9002.1325 1125240 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Grenzschichtprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b67b-72a3a23ad397/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

