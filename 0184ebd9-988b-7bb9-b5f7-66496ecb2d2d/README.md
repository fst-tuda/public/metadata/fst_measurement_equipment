## Sensor PU25

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b5f7-66496ecb2d2d`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b5f7-66496ecb2d2d)
### UUID: `0184ebd9-988b-7bb9-b5f7-66496ecb2d2d`
### identifier: `fst-inv:D46`

</div>

## Keywords: Druck

## General Info

| property | value |
|-:|:-|
| comment: | defekt |
| manufacturer: | Halstrup Multur |
| name: | PU25 |
| serial number: | 140488020070 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Grenzschichtprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b5f7-66496ecb2d2d/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

