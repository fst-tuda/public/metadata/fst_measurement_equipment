## Sensor Deltabar S

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-8440-b70567857b98`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-8440-b70567857b98)
### UUID: `0184ebd9-988b-7bba-8440-b70567857b98`
### identifier: `fst-inv:D101`

</div>

## Keywords: Druck, relativ

## General Info

| property | value |
|-:|:-|
| comment: | länger eingelagert |
| manufacturer: | Endress+Hausser |
| name: | Deltabar S |
| serial number: | K406400109D |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | None |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-8440-b70567857b98/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

