## Sensor PR-33X/3bar

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8e98-dae60b80211c`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8e98-dae60b80211c)
### UUID: `0184ebd9-988c-7bbb-8e98-dae60b80211c`
### identifier: `fst-inv:D120`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | neu |
| manufacturer: | Keller |
| name: | PR-33X/3bar |
| serial number: | 1090843 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | None |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-8e98-dae60b80211c/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

