## Sensor PAA-23/8465-10

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-bb1e-fc5a5991f852`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-bb1e-fc5a5991f852)
### UUID: `0184ebd9-988b-7bb9-bb1e-fc5a5991f852`
### identifier: `fst-inv:D66`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-23/8465-10 |
| serial number: | 73154 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Normprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-bb1e-fc5a5991f852/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

