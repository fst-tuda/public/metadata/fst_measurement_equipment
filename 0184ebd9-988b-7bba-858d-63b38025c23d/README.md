## Sensor PD-23/8666-0.2

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-858d-63b38025c23d`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-858d-63b38025c23d)
### UUID: `0184ebd9-988b-7bba-858d-63b38025c23d`
### identifier: `fst-inv:D106`

</div>

## Keywords: Druck, Piezoresistiv, relativ

## General Info

| property | value |
|-:|:-|
| comment: | vor Gebrauch kalibrieren |
| manufacturer: | Keller |
| name: | PD-23/8666-0.2 |
| serial number: | 80876 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | None |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-858d-63b38025c23d/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

