## Sensor PAA-33X/80794

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-bf0b-8ac992cecf10`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-bf0b-8ac992cecf10)
### UUID: `0184ebd9-988b-7bb9-bf0b-8ac992cecf10`
### identifier: `fst-inv:D081`

</div>

## Keywords: Druck, Piezoresistiv, absolut

<img width="400" src=https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/raw/main/0184ebd9-988b-7bb9-bf0b-8ac992cecf10/img/33x%402x.jpg>

<img width="400" src=https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/raw/main/0184ebd9-988b-7bb9-bf0b-8ac992cecf10/img/33x-binderstecker-26%402x.jpg>

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-33X/80794 |
| serial number: | 428736 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-bf0b-8ac992cecf10/docs/) |
| documentation: | [docs/Datenblatt_Serie-33X_g.pdf](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-bf0b-8ac992cecf10/docs/Datenblatt_Serie-33X_g.pdf) |
| documentation: | [docs/info_160105_Keller_PAA_33X_30bar_80794_SN_428736_Inv_Nr._000520_Ludwig.pdf](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-bf0b-8ac992cecf10/docs/info_160105_Keller_PAA_33X_30bar_80794_SN_428736_Inv_Nr._000520_Ludwig.pdf) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

