## Sensor FEH631.Y0.S1.0015.M1. …

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0193081d-09ea-769c-8c61-dfe8ec4c19fc`](https://w3id.org/fst/resource/0193081d-09ea-769c-8c61-dfe8ec4c19fc)
### UUID: `0193081d-09ea-769c-8c61-dfe8ec4c19fc`
### identifier: `fst-inv:V21`

</div>

## Keywords: MID, Volumenstrom

## General Info

| property | value |
|-:|:-|
| comment: | 
Montageort: Verbraucher 1, Sensor hat einen realen Ausgabebereich von 0.004 A - 0.02 A mit einer Signalumwandlung über einen Trennverstärker |
| manufacturer: | ABB |
| name: | FEH631.Y0.S1.0015.M1. … |
| serial number: | 3K220000980477 |
| used procedure: | MID |
|-|-|
| owner: | FST |
| maintainer: | Logan |
| last known location: | Resilienzdemonstrator |
| last modification: | 2024-02-20T00:00:00 |
|-|-|
| related resources: | Trennverstärker |
| documentation: | [file:///home/sebastian/Desktop/fst/projects/hydropulser-database-scripts/_generated/pID_directories/0193081d-09ea-769c-8c61-dfe8ec4c19fc/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0193081d-09ea-769c-8c61-dfe8ec4c19fc/file:///home/sebastian/Desktop/fst/projects/hydropulser-database-scripts/_generated/pID_directories/0193081d-09ea-769c-8c61-dfe8ec4c19fc/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

