## Sensor PA23 200bar

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-8365-2c49aaa69770`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-8365-2c49aaa69770)
### UUID: `0184ebd9-988b-7bba-8365-2c49aaa69770`
### identifier: `fst-inv:D097`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PA23 200bar |
| serial number: | None |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Prüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-8365-2c49aaa69770/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

