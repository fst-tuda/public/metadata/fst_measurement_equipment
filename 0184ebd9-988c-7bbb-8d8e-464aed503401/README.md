## Sensor PE200

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8d8e-464aed503401`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8d8e-464aed503401)
### UUID: `0184ebd9-988c-7bbb-8d8e-464aed503401`
### identifier: `fst-inv:D116`

</div>

## Keywords: Druck, Überdruck, Überdruckmessgerät

## General Info

| property | value |
|-:|:-|
| comment: | vor Gebrauch kalibrieren |
| manufacturer: | HBM |
| name: | PE200 |
| serial number: | D66604 |
| used procedure: | Überdruckmessgerät |
|-|-|
| owner: | FST |
| maintainer: | None |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | Anleitung |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-8d8e-464aed503401/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

