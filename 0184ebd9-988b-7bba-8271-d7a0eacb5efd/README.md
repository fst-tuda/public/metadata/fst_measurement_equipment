## Sensor PAA-33X/10bar

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-8271-d7a0eacb5efd`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-8271-d7a0eacb5efd)
### UUID: `0184ebd9-988b-7bba-8271-d7a0eacb5efd`
### identifier: `fst-inv:D093`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-33X/10bar |
| serial number: | 1011233 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-8271-d7a0eacb5efd/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

