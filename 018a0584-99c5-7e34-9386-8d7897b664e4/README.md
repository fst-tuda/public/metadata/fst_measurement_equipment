## Sensor A-10

<div align="right">

### IRI: [`https://w3id.org/fst/resource/018a0584-99c5-7e34-9386-8d7897b664e4`](https://w3id.org/fst/resource/018a0584-99c5-7e34-9386-8d7897b664e4)
### UUID: `018a0584-99c5-7e34-9386-8d7897b664e4`
### identifier: `fst-inv:D129`

</div>

## Keywords: Druck, absolut

## General Info

| property | value |
|-:|:-|
| comment: | 
neu |
| manufacturer: | WIKA |
| name: | A-10 |
| serial number: | 1A028EUETJ1 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [file:///home/sebastian/Desktop/hydropulser-database-scripts/_generated/pID_directories/018a0584-99c5-7e34-9386-8d7897b664e4/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/018a0584-99c5-7e34-9386-8d7897b664e4/file:///home/sebastian/Desktop/hydropulser-database-scripts/_generated/pID_directories/018a0584-99c5-7e34-9386-8d7897b664e4/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

