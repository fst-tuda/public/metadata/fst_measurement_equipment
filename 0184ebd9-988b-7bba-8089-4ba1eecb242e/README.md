## Sensor PAA-23/8465.1-4

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-8089-4ba1eecb242e`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-8089-4ba1eecb242e)
### UUID: `0184ebd9-988b-7bba-8089-4ba1eecb242e`
### identifier: `fst-inv:D087`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-23/8465.1-4 |
| serial number: | 22588 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-8089-4ba1eecb242e/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

