## Sensor PAA-23/8465-5

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-bb87-646bbaa9a20e`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-bb87-646bbaa9a20e)
### UUID: `0184ebd9-988b-7bb9-bb87-646bbaa9a20e`
### identifier: `fst-inv:D67`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-23/8465-5 |
| serial number: | 4954 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Normprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-bb87-646bbaa9a20e/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

