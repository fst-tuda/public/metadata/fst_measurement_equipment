## Sensor U10M/5kN

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-97cd-63e191335f56`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-97cd-63e191335f56)
### UUID: `0184ebd9-988c-7bbb-97cd-63e191335f56`
### identifier: `fst-inv:K09`

</div>

## Keywords: DMS, Kraft

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | HBM |
| name: | U10M/5kN |
| serial number: | M0052128686765n |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-97cd-63e191335f56/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

