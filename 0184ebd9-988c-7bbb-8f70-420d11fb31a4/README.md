## Sensor PD-33X/10bar

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8f70-420d11fb31a4`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8f70-420d11fb31a4)
### UUID: `0184ebd9-988c-7bbb-8f70-420d11fb31a4`
### identifier: `fst-inv:D123`

</div>

## Keywords: Druck, Piezoresistiv, relativ

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PD-33X/10bar |
| serial number: | 1226639 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Wetterich |
| last known location: | Sirup Mischanlage |
| last modification: | 2021-02-17T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-8f70-420d11fb31a4/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

