## Sensor 1-S9M/10KN-1

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-95cd-c16f369c5d01`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-95cd-c16f369c5d01)
### UUID: `0184ebd9-988c-7bbb-95cd-c16f369c5d01`
### identifier: `fst-inv:K02`

</div>

## Keywords: DMS, Kraft

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | HBM |
| name: | 1-S9M/10KN-1 |
| serial number: | 30893194 |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-95cd-c16f369c5d01/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

