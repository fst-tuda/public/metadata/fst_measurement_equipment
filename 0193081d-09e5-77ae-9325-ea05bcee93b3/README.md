## Sensor PD-33X/80920

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0193081d-09e5-77ae-9325-ea05bcee93b3`](https://w3id.org/fst/resource/0193081d-09e5-77ae-9325-ea05bcee93b3)
### UUID: `0193081d-09e5-77ae-9325-ea05bcee93b3`
### identifier: `fst-inv:D136`

</div>

## Keywords: Druck, Piezoresistiv, relativ

## General Info

| property | value |
|-:|:-|
| comment: | 
Montageort: Pumpe 2, Strang 2 |
| manufacturer: | Keller |
| name: | PD-33X/80920 |
| serial number: | 1664525 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Logan |
| last known location: | Resilienzdemonstrator |
| last modification: | 2023-12-08T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [file:///home/sebastian/Desktop/fst/projects/hydropulser-database-scripts/_generated/pID_directories/0193081d-09e5-77ae-9325-ea05bcee93b3/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0193081d-09e5-77ae-9325-ea05bcee93b3/file:///home/sebastian/Desktop/fst/projects/hydropulser-database-scripts/_generated/pID_directories/0193081d-09e5-77ae-9325-ea05bcee93b3/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

