## Sensor KTE56200GQ4

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-81be-82f24102228f`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-81be-82f24102228f)
### UUID: `0184ebd9-988b-7bba-81be-82f24102228f`
### identifier: `fst-inv:D090`

</div>

## Keywords: Druck, Piezoresistiv, relativ

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Sensortechnics |
| name: | KTE56200GQ4 |
| serial number: | None |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-81be-82f24102228f/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

