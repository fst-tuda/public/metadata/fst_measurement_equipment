## Sensor n.e.

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b196-99fb4381823b`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b196-99fb4381823b)
### UUID: `0184ebd9-988b-7bb9-b196-99fb4381823b`
### identifier: `fst-inv:D29`

</div>

## Keywords: DMS, Druck

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Burster |
| name: | n.e. |
| serial number: | n.e. |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b196-99fb4381823b/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

