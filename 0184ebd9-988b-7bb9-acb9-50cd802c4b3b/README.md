## Sensor PA-23/8465-100

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-acb9-50cd802c4b3b`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-acb9-50cd802c4b3b)
### UUID: `0184ebd9-988b-7bb9-acb9-50cd802c4b3b`
### identifier: `fst-inv:D10`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PA-23/8465-100 |
| serial number: | 19175 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-acb9-50cd802c4b3b/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

