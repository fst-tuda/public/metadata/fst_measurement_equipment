## Sensor ETS 4146-B-006-000

<div align="right">

### IRI: [`https://w3id.org/fst/resource/064f05d1-5d2d-7a6f-8000-a3da10f5a1a3`](https://w3id.org/fst/resource/064f05d1-5d2d-7a6f-8000-a3da10f5a1a3)
### UUID: `064f05d1-5d2d-7a6f-8000-a3da10f5a1a3`
### identifier: `fst-inv:T24`

</div>

## Keywords: Pt1000, Temperatur

## General Info

<img align="right" width="400" src=/064f05d1-5d2d-7a6f-8000-a3da10f5a1a3/img/hydac_ETS_4146_b_006_000__648P016436.jpg>

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Hydac |
| name: | ETS 4146-B-006-000 |
| serial number: | 648P016436 |
| used procedure: | Pt1000 |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Prüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [file:///home/linuxlite/Desktop/hydropulser-database-scripts/_generated/pID_directories/064f05d1-5d2d-7a6f-8000-a3da10f5a1a3/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/064f05d1-5d2d-7a6f-8000-a3da10f5a1a3/file:///home/linuxlite/Desktop/hydropulser-database-scripts/_generated/pID_directories/064f05d1-5d2d-7a6f-8000-a3da10f5a1a3/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

