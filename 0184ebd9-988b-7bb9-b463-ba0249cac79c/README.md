## Sensor LBA250B

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b463-ba0249cac79c`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b463-ba0249cac79c)
### UUID: `0184ebd9-988b-7bb9-b463-ba0249cac79c`
### identifier: `fst-inv:D40`

</div>

## Keywords: Differenzdruck, Druck

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Sensortechnics |
| name: | LBA250B |
| serial number: | None |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Grenzschichtprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b463-ba0249cac79c/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

