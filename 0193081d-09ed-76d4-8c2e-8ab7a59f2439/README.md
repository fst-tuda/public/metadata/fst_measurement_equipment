## Sensor Pw-MU Serie 908G

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0193081d-09ed-76d4-8c2e-8ab7a59f2439`](https://w3id.org/fst/resource/0193081d-09ed-76d4-8c2e-8ab7a59f2439)
### UUID: `0193081d-09ed-76d4-8c2e-8ab7a59f2439`
### identifier: `fst-inv:O20`

</div>

## Keywords: Leistung, Stromwandlung und Spannungsteilung

## General Info

| property | value |
|-:|:-|
| comment: | 
Montageort: Schaltschrank |
| manufacturer: | Müller + Ziegler |
| name: | Pw-MU Serie 908G |
| serial number: | 2365 |
| used procedure: | Stromwandlung und Spannungsteilung |
|-|-|
| owner: | FST |
| maintainer: | Logan |
| last known location: | Resilienzdemonstrator |
| last modification: | UNKNOWN |
|-|-|
| related resources: | None |
| documentation: | [file:///home/sebastian/Desktop/fst/projects/hydropulser-database-scripts/_generated/pID_directories/0193081d-09ed-76d4-8c2e-8ab7a59f2439/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0193081d-09ed-76d4-8c2e-8ab7a59f2439/file:///home/sebastian/Desktop/fst/projects/hydropulser-database-scripts/_generated/pID_directories/0193081d-09ed-76d4-8c2e-8ab7a59f2439/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

