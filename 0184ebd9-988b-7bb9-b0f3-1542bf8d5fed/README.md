## Sensor PR 22, 3903-18-33.00

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b0f3-1542bf8d5fed`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b0f3-1542bf8d5fed)
### UUID: `0184ebd9-988b-7bb9-b0f3-1542bf8d5fed`
### identifier: `fst-inv:D26`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Hydrotechnik GmbH |
| name: | PR 22, 3903-18-33.00 |
| serial number: | C6243S |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b0f3-1542bf8d5fed/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

