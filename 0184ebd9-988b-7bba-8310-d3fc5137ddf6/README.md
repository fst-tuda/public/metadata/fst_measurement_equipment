## Sensor PA23/8465-200

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-8310-d3fc5137ddf6`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-8310-d3fc5137ddf6)
### UUID: `0184ebd9-988b-7bba-8310-d3fc5137ddf6`
### identifier: `fst-inv:D096`

</div>

## Keywords: Druck, Piezoresistiv, relativ

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PA23/8465-200 |
| serial number: | 25185 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Prüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-8310-d3fc5137ddf6/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

