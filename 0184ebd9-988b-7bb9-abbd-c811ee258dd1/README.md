## Sensor PAA-23/8465.1-10

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-abbd-c811ee258dd1`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-abbd-c811ee258dd1)
### UUID: `0184ebd9-988b-7bb9-abbd-c811ee258dd1`
### identifier: `fst-inv:D06`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-23/8465.1-10 |
| serial number: | 22563 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-abbd-c811ee258dd1/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

