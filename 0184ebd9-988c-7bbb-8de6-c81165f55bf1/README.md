## Sensor PAA-33X/10bar

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8de6-c81165f55bf1`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8de6-c81165f55bf1)
### UUID: `0184ebd9-988c-7bbb-8de6-c81165f55bf1`
### identifier: `fst-inv:D117`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | neu |
| manufacturer: | Keller |
| name: | PAA-33X/10bar |
| serial number: | 1011240 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Wetterich |
| last known location: | Sirup Mischanlage |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-8de6-c81165f55bf1/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

