## Sensor PU10

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b50c-824cdbe7c73f`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b50c-824cdbe7c73f)
### UUID: `0184ebd9-988b-7bb9-b50c-824cdbe7c73f`
### identifier: `fst-inv:D43`

</div>

## Keywords: Druck

## General Info

| property | value |
|-:|:-|
| comment: | ohne Netzstecker |
| manufacturer: | Halstrup Multur |
| name: | PU10 |
| serial number: | 291087020063 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Grenzschichtprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b50c-824cdbe7c73f/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

