## Sensor ALF300UFR0K0

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9802-abdb21f0289c`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9802-abdb21f0289c)
### UUID: `0184ebd9-988c-7bbb-9802-abdb21f0289c`
### identifier: `fst-inv:K10`

</div>

## Keywords: DMS, Kraft

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Althen |
| name: | ALF300UFR0K0 |
| serial number: | 50432 |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-9802-abdb21f0289c/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

