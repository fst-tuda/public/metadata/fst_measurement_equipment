## Sensor PAA-23/8465-5

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-beb8-93b91fedf6fd`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-beb8-93b91fedf6fd)
### UUID: `0184ebd9-988b-7bb9-beb8-93b91fedf6fd`
### identifier: `fst-inv:D079`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-23/8465-5 |
| serial number: | 88625 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-beb8-93b91fedf6fd/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

