## Sensor K -25..150°C

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-a2c0-8ba93f63085c`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-a2c0-8ba93f63085c)
### UUID: `0184ebd9-988c-7bbb-a2c0-8ba93f63085c`
### identifier: `fst-inv:T14`

</div>

## Keywords: Messumformer/Thermoelement Typ K, Temperatur

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | LKM |
| name: | K -25..150°C |
| serial number: | 163616 |
| used procedure: | Messumformer/Thermoelement Typ K |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-a2c0-8ba93f63085c/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

