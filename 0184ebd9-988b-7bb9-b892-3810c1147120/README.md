## Sensor PBMN

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b892-3810c1147120`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b892-3810c1147120)
### UUID: `0184ebd9-988b-7bb9-b892-3810c1147120`
### identifier: `fst-inv:D56`

</div>

## Keywords: DMS, Druck, absolut

## General Info

| property | value |
|-:|:-|
| comment: | PBMN-2.5.B26.A.A2.44.06.2.1.1.000.000 |
| manufacturer: | Baumer GmbH |
| name: | PBMN |
| serial number: | 2.17.101929865.2 |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Schänzle |
| last known location: | Hydraulikpumpenprüfstand |
| last modification: | 2017-09-01T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b892-3810c1147120/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

