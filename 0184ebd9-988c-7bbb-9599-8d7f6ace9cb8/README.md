## Sensor 661.19F-01

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9599-8d7f6ace9cb8`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9599-8d7f6ace9cb8)
### UUID: `0184ebd9-988c-7bbb-9599-8d7f6ace9cb8`
### identifier: `fst-inv:K01`

</div>

## Keywords: DMS, Kraft

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | MTS |
| name: | 661.19F-01 |
| serial number: | 369955 |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-9599-8d7f6ace9cb8/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

