## Sensor dTrans T02j

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0193081d-09ee-7f00-b9ec-5dd579240574`](https://w3id.org/fst/resource/0193081d-09ee-7f00-b9ec-5dd579240574)
### UUID: `0193081d-09ee-7f00-b9ec-5dd579240574`
### identifier: `fst-inv:T22`

</div>

## Keywords: Messumformer für PT100, Temperatur

## General Info

| property | value |
|-:|:-|
| comment: | 
Montageort: Ausfluss Reservoir/Schaltschrank |
| manufacturer: | Jumo |
| name: | dTrans T02j |
| serial number: | 256613801018450004 |
| used procedure: | Messumformer für PT100 |
|-|-|
| owner: | FST |
| maintainer: | Logan |
| last known location: | Resilienzdemonstrator |
| last modification: | None |
|-|-|
| related resources: | PT100 1/3 DIN 4-Leiterschaltung |
| documentation: | [file:///home/sebastian/Desktop/fst/projects/hydropulser-database-scripts/_generated/pID_directories/0193081d-09ee-7f00-b9ec-5dd579240574/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0193081d-09ee-7f00-b9ec-5dd579240574/file:///home/sebastian/Desktop/fst/projects/hydropulser-database-scripts/_generated/pID_directories/0193081d-09ee-7f00-b9ec-5dd579240574/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

