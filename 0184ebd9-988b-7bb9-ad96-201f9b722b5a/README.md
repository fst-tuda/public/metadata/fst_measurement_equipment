## Sensor PL 207, 423-207-000-041

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-ad96-201f9b722b5a`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-ad96-201f9b722b5a)
### UUID: `0184ebd9-988b-7bb9-ad96-201f9b722b5a`
### identifier: `fst-inv:D14`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | vibro-meter |
| name: | PL 207, 423-207-000-041 |
| serial number: | 229 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-ad96-201f9b722b5a/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

