## Sensor PSI 9016

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b714-a7f1e6e6216d`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b714-a7f1e6e6216d)
### UUID: `0184ebd9-988b-7bb9-b714-a7f1e6e6216d`
### identifier: `fst-inv:D50`

</div>

## Keywords: Differenzdruck, Druck, relativ

## General Info

| property | value |
|-:|:-|
| comment: | 6 Kanäle, zusammen mit D51 |
| manufacturer: | Pressure Systems, Inc. |
| name: | PSI 9016 |
| serial number: | S/N 1648 |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Saul |
| last known location: | Ventilatorprüfstände |
| last modification: | 2014-08-21T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b714-a7f1e6e6216d/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

