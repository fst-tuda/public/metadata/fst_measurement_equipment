## Sensor K 0..100°C

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-a319-f28d8782ed65`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-a319-f28d8782ed65)
### UUID: `0184ebd9-988c-7bbb-a319-f28d8782ed65`
### identifier: `fst-inv:T15`

</div>

## Keywords: Messumformer/Thermoelement Typ K, Temperatur

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | LKM |
| name: | K 0..100°C |
| serial number: | 243008 |
| used procedure: | Messumformer/Thermoelement Typ K |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-a319-f28d8782ed65/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

