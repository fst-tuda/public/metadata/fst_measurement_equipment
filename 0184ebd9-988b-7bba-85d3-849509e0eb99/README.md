## Sensor PAA-23/8465-5

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-85d3-849509e0eb99`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-85d3-849509e0eb99)
### UUID: `0184ebd9-988b-7bba-85d3-849509e0eb99`
### identifier: `fst-inv:D107`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | vor Gebrauch kalibrieren |
| manufacturer: | Keller |
| name: | PAA-23/8465-5 |
| serial number: | 4953 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | None |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-85d3-849509e0eb99/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

