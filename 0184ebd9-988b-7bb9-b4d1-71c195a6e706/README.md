## Sensor PU25

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b4d1-71c195a6e706`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b4d1-71c195a6e706)
### UUID: `0184ebd9-988b-7bb9-b4d1-71c195a6e706`
### identifier: `fst-inv:D42`

</div>

## Keywords: Druck

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Halstrup Multur |
| name: | PU25 |
| serial number: | 140488020068 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Grenzschichtprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b4d1-71c195a6e706/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

