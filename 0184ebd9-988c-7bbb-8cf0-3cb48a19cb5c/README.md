## Sensor EDS 3446-1-0400-000

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8cf0-3cb48a19cb5c`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8cf0-3cb48a19cb5c)
### UUID: `0184ebd9-988c-7bbb-8cf0-3cb48a19cb5c`
### identifier: `fst-inv:D113`

</div>

## Keywords: Druck, Druckschalter, Schalter

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | HYDAC |
| name: | EDS 3446-1-0400-000 |
| serial number: | 745E115955 |
| used procedure: | Druckschalter |
|-|-|
| owner: | FST |
| maintainer: | None |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | Anschluss für 9V Batterie |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-8cf0-3cb48a19cb5c/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

