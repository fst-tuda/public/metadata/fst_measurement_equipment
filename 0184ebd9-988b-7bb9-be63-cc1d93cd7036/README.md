## Sensor PA-23 400bar

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-be63-cc1d93cd7036`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-be63-cc1d93cd7036)
### UUID: `0184ebd9-988b-7bb9-be63-cc1d93cd7036`
### identifier: `fst-inv:D078`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PA-23 400bar |
| serial number: | 74907 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-be63-cc1d93cd7036/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

