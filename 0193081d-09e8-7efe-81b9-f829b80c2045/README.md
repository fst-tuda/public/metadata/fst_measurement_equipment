## Sensor Cerabar S PMP71

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0193081d-09e8-7efe-81b9-f829b80c2045`](https://w3id.org/fst/resource/0193081d-09e8-7efe-81b9-f829b80c2045)
### UUID: `0193081d-09e8-7efe-81b9-f829b80c2045`
### identifier: `fst-inv:D149`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | 
Montageort: kleiner Tank, Strang 2, Sensor hat einen realen Ausgabebereich von 0.004 A - 0.02 A mit einer Signalumwandlung über einen Widerstand |
| manufacturer: | E+H |
| name: | Cerabar S PMP71 |
| serial number: | H1068B0109C |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Logan |
| last known location: | Resilienzdemonstrator |
| last modification: | 2013-01-19T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [file:///home/sebastian/Desktop/fst/projects/hydropulser-database-scripts/_generated/pID_directories/0193081d-09e8-7efe-81b9-f829b80c2045/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0193081d-09e8-7efe-81b9-f829b80c2045/file:///home/sebastian/Desktop/fst/projects/hydropulser-database-scripts/_generated/pID_directories/0193081d-09e8-7efe-81b9-f829b80c2045/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

