## Sensor PBMN

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b872-5c2f18e5cd3b`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b872-5c2f18e5cd3b)
### UUID: `0184ebd9-988b-7bb9-b872-5c2f18e5cd3b`
### identifier: `fst-inv:D55`

</div>

## Keywords: DMS, Druck, absolut

## General Info

| property | value |
|-:|:-|
| comment: | PBMN-2.5.B18.A.A2.44.06.2.1.1.000.000 |
| manufacturer: | Baumer GmbH |
| name: | PBMN |
| serial number: | 2.17.101929867.2 |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Schänzle |
| last known location: | Hydraulikpumpenprüfstand |
| last modification: | 2017-09-01T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b872-5c2f18e5cd3b/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

