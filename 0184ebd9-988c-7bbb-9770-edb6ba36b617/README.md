## Sensor U10M/50kN

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9770-edb6ba36b617`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9770-edb6ba36b617)
### UUID: `0184ebd9-988c-7bbb-9770-edb6ba36b617`
### identifier: `fst-inv:K07`

</div>

## Keywords: DMS, Kraft

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | HBM |
| name: | U10M/50kN |
| serial number: | 0501518386550k |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-9770-edb6ba36b617/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

