## Sensor SM10-A-SA

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-90d4-1ba867e7a4fb`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-90d4-1ba867e7a4fb)
### UUID: `0184ebd9-988c-7bbb-90d4-1ba867e7a4fb`
### identifier: `fst-inv:W01`

</div>

## Keywords: Induktiv, Weg

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | WayCon |
| name: | SM10-A-SA |
| serial number: | 826026 |
| used procedure: | Induktiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-90d4-1ba867e7a4fb/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

