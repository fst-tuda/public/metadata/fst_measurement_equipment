## Sensor PL 205, 423-205-000-041

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-ac58-3d794672b694`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-ac58-3d794672b694)
### UUID: `0184ebd9-988b-7bb9-ac58-3d794672b694`
### identifier: `fst-inv:D09`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | vibro-meter |
| name: | PL 205, 423-205-000-041 |
| serial number: | 138 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-ac58-3d794672b694/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

