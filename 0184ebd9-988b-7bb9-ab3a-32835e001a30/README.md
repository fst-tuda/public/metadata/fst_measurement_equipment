## Sensor 290E

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-ab3a-32835e001a30`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-ab3a-32835e001a30)
### UUID: `0184ebd9-988b-7bb9-ab3a-32835e001a30`
### identifier: `fst-inv:D05`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | setra |
| name: | 290E |
| serial number: | 135530 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-ab3a-32835e001a30/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

