## Sensor PD-23/8666 -20/+20

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-815a-cd1b41f3fba6`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-815a-cd1b41f3fba6)
### UUID: `0184ebd9-988b-7bba-815a-cd1b41f3fba6`
### identifier: `fst-inv:D089`

</div>

## Keywords: Differenzdruck, Druck, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PD-23/8666 -20/+20 |
| serial number: | 74999 |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-815a-cd1b41f3fba6/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

