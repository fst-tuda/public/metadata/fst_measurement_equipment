## Sensor ETS 4146-B-006-000

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-a38e-1797d4efe338`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-a38e-1797d4efe338)
### UUID: `0184ebd9-988c-7bbb-a38e-1797d4efe338`
### identifier: `fst-inv:T17`

</div>

## Keywords: Pt100, Temperatur

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Hydac |
| name: | ETS 4146-B-006-000 |
| serial number: | 827P022412 |
| used procedure: | Pt100 |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Prüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-a38e-1797d4efe338/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

