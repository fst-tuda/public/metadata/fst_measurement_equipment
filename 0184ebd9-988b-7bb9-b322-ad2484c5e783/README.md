## Sensor A08 XHT

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b322-ad2484c5e783`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b322-ad2484c5e783)
### UUID: `0184ebd9-988b-7bb9-b322-ad2484c5e783`
### identifier: `fst-inv:D35`

</div>

## Keywords: Druck

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | STW |
| name: | A08 XHT |
| serial number: | 03.003196.2 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Kuhr |
| last known location: | Büro 471 |
| last modification: | None |
|-|-|
| related resources: | WIKA Manometer 0 - 16 bar |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b322-ad2484c5e783/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

