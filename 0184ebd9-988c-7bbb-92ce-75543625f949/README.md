## Sensor TS-0025

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-92ce-75543625f949`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-92ce-75543625f949)
### UUID: `0184ebd9-988c-7bbb-92ce-75543625f949`
### identifier: `fst-inv:W08`

</div>

## Keywords: Mechanisch, Weg

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Novotechnik |
| name: | TS-0025 |
| serial number: | 130581 |
| used procedure: | Mechanisch |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-92ce-75543625f949/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

