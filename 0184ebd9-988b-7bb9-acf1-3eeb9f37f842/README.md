## Sensor PA-23/8465-100

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-acf1-3eeb9f37f842`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-acf1-3eeb9f37f842)
### UUID: `0184ebd9-988b-7bb9-acf1-3eeb9f37f842`
### identifier: `fst-inv:D11`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PA-23/8465-100 |
| serial number: | 74905 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-acf1-3eeb9f37f842/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

