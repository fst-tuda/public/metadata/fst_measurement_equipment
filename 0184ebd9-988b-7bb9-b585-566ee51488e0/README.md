## Sensor PU2,5

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b585-566ee51488e0`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b585-566ee51488e0)
### UUID: `0184ebd9-988b-7bb9-b585-566ee51488e0`
### identifier: `fst-inv:D45`

</div>

## Keywords: Druck

## General Info

| property | value |
|-:|:-|
| comment: | ohne Netzstecker |
| manufacturer: | Halstrup Multur |
| name: | PU2,5 |
| serial number: | 120490020056 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Grenzschichtprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b585-566ee51488e0/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

