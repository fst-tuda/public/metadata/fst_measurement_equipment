## Sensor O-10 (T) 

<div align="right">

### IRI: [`https://w3id.org/fst/resource/018a058b-c091-7d2e-a809-52a2e0fb9dc6`](https://w3id.org/fst/resource/018a058b-c091-7d2e-a809-52a2e0fb9dc6)
### UUID: `018a058b-c091-7d2e-a809-52a2e0fb9dc6`
### identifier: `fst-inv:D132`

</div>

## Keywords: Druck, relativ

## General Info

| property | value |
|-:|:-|
| comment: | 
neu |
| manufacturer: | WIKA |
| name: | O-10 (T)  |
| serial number: | 110J2CYM |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [file:///home/sebastian/Desktop/hydropulser-database-scripts/_generated/pID_directories/018a058b-c091-7d2e-a809-52a2e0fb9dc6/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/018a058b-c091-7d2e-a809-52a2e0fb9dc6/file:///home/sebastian/Desktop/hydropulser-database-scripts/_generated/pID_directories/018a058b-c091-7d2e-a809-52a2e0fb9dc6/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

