## Sensor PAA-33X / 80794

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b997-42d6c7f54c0d`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b997-42d6c7f54c0d)
### UUID: `0184ebd9-988b-7bb9-b997-42d6c7f54c0d`
### identifier: `fst-inv:D60`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-33X / 80794 |
| serial number: | 867454 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Kuhr |
| last known location: | Gleitlagerprüfstand |
| last modification: | 2018-09-05T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b997-42d6c7f54c0d/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

