## Sensor PA-25TAB/80087

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-afc2-f3ecf01ec673`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-afc2-f3ecf01ec673)
### UUID: `0184ebd9-988b-7bb9-afc2-f3ecf01ec673`
### identifier: `fst-inv:D23`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PA-25TAB/80087 |
| serial number: | 60036 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-afc2-f3ecf01ec673/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

