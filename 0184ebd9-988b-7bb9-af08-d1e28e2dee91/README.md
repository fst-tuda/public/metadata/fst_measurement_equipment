## Sensor BHL-4201-00-03

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-af08-d1e28e2dee91`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-af08-d1e28e2dee91)
### UUID: `0184ebd9-988b-7bb9-af08-d1e28e2dee91`
### identifier: `fst-inv:D20`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | TransInstruments |
| name: | BHL-4201-00-03 |
| serial number: | L423875 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-af08-d1e28e2dee91/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

