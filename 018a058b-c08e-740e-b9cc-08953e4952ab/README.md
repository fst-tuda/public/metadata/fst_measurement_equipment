## Sensor TYP 102

<div align="right">

### IRI: [`https://w3id.org/fst/resource/018a058b-c08e-740e-b9cc-08953e4952ab`](https://w3id.org/fst/resource/018a058b-c08e-740e-b9cc-08953e4952ab)
### UUID: `018a058b-c08e-740e-b9cc-08953e4952ab`
### identifier: `fst-inv:T23`

</div>

## Keywords: Messumformer/Thermoelement Typ K, Temperatur

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | LKM |
| name: | TYP 102 |
| serial number: | 184416 Kennnummer |
| used procedure: | Messumformer/Thermoelement Typ K |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/018a058b-c08e-740e-b9cc-08953e4952ab/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

