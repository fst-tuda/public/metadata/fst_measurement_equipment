## Sensor PXM309-007A

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b693-580859d3a398`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b693-580859d3a398)
### UUID: `0184ebd9-988b-7bb9-b693-580859d3a398`
### identifier: `fst-inv:D48`

</div>

## Keywords: Druck, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Omega Engineering GmbH |
| name: | PXM309-007A |
| serial number: | DE022813I024 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Saul |
| last known location: | Druckkammerprüfstand |
| last modification: | 2017-09-04T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b693-580859d3a398/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

