## Sensor PAA-35X/80797

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-8402-16963d276842`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-8402-16963d276842)
### UUID: `0184ebd9-988b-7bba-8402-16963d276842`
### identifier: `fst-inv:D100`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-35X/80797 |
| serial number: | 126240 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-8402-16963d276842/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

