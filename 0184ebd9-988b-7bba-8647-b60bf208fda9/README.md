## Sensor PD-23/8666.1-4

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-8647-b60bf208fda9`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-8647-b60bf208fda9)
### UUID: `0184ebd9-988b-7bba-8647-b60bf208fda9`
### identifier: `fst-inv:D108`

</div>

## Keywords: Druck, Piezoresistiv, relativ

## General Info

| property | value |
|-:|:-|
| comment: | vor Gebrauch kalibrieren |
| manufacturer: | Keller |
| name: | PD-23/8666.1-4 |
| serial number: | 82894 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | None |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-8647-b60bf208fda9/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

