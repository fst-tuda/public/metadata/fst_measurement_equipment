## Sensor LWG150

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-93a3-8a97bf0e7983`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-93a3-8a97bf0e7983)
### UUID: `0184ebd9-988c-7bbb-93a3-8a97bf0e7983`
### identifier: `fst-inv:W11`

</div>

## Keywords: Induktiv, Weg

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Novotechnik |
| name: | LWG150 |
| serial number: | None |
| used procedure: | Induktiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-93a3-8a97bf0e7983/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

