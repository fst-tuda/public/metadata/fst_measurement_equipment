## Sensor PAA-33X/80794

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-baf0-a558c2948e57`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-baf0-a558c2948e57)
### UUID: `0184ebd9-988b-7bb9-baf0-a558c2948e57`
### identifier: `fst-inv:D65`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-33X/80794 |
| serial number: | 149756 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Normprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-baf0-a558c2948e57/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

