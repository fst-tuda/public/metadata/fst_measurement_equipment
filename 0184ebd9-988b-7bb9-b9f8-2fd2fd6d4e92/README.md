## Sensor PD-23 / 8666

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b9f8-2fd2fd6d4e92`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b9f8-2fd2fd6d4e92)
### UUID: `0184ebd9-988b-7bb9-b9f8-2fd2fd6d4e92`
### identifier: `fst-inv:D61`

</div>

## Keywords: Differenzdruck, Druck, relativ

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PD-23 / 8666 |
| serial number: | 907130 |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Kuhr |
| last known location: | Gleitlagerprüfstand |
| last modification: | 2018-09-21T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b9f8-2fd2fd6d4e92/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

