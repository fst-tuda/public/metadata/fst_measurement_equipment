## Sensor PD-23/8666

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8bc3-1af09f533c5f`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-8bc3-1af09f533c5f)
### UUID: `0184ebd9-988c-7bbb-8bc3-1af09f533c5f`
### identifier: `fst-inv:D110`

</div>

## Keywords: Druck, Piezoresistiv, relativ

## General Info

| property | value |
|-:|:-|
| comment: | vor Gebrauch kalibrieren |
| manufacturer: | Keller |
| name: | PD-23/8666 |
| serial number: | 86972 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | None |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-8bc3-1af09f533c5f/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

