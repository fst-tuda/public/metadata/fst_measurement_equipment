## Sensor U2A

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-96c9-73a6972fe09e`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-96c9-73a6972fe09e)
### UUID: `0184ebd9-988c-7bbb-96c9-73a6972fe09e`
### identifier: `fst-inv:K06`

</div>

## Keywords: DMS, Kraft

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | HBM |
| name: | U2A |
| serial number: | D29507 1t |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-96c9-73a6972fe09e/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

