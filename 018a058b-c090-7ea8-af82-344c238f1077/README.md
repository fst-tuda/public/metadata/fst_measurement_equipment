## Sensor A-10

<div align="right">

### IRI: [`https://w3id.org/fst/resource/018a058b-c090-7ea8-af82-344c238f1077`](https://w3id.org/fst/resource/018a058b-c090-7ea8-af82-344c238f1077)
### UUID: `018a058b-c090-7ea8-af82-344c238f1077`
### identifier: `fst-inv:D131`

</div>

## Keywords: Druck, absolut

## General Info

| property | value |
|-:|:-|
| comment: | 
neu |
| manufacturer: | WIKA |
| name: | A-10 |
| serial number: | 1A028EU6625 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [file:///home/sebastian/Desktop/hydropulser-database-scripts/_generated/pID_directories/018a058b-c090-7ea8-af82-344c238f1077/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/018a058b-c090-7ea8-af82-344c238f1077/file:///home/sebastian/Desktop/hydropulser-database-scripts/_generated/pID_directories/018a058b-c090-7ea8-af82-344c238f1077/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

