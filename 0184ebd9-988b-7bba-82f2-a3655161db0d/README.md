## Sensor PA23/8465-200

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-82f2-a3655161db0d`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-82f2-a3655161db0d)
### UUID: `0184ebd9-988b-7bba-82f2-a3655161db0d`
### identifier: `fst-inv:D095`

</div>

## Keywords: Druck, Piezoresistiv, relativ

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PA23/8465-200 |
| serial number: | 25186 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Prüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [file:///home/linuxlite/Desktop/hydropulser-database-scripts/_generated/pID_directories/0184ebd9-988b-7bba-82f2-a3655161db0d/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-82f2-a3655161db0d/file:///home/linuxlite/Desktop/hydropulser-database-scripts/_generated/pID_directories/0184ebd9-988b-7bba-82f2-a3655161db0d/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

