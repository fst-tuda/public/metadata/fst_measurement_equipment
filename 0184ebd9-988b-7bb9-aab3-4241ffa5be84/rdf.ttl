PREFIX dbo: <https://dbpedia.org/ontology/>
PREFIX dcmitype: <http://purl.org/dc/dcmitype/>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX fst: <https://w3id.org/fst/resource/>
PREFIX quantitykind: <https://qudt.org/vocab/quantitykind/>
PREFIX qudt: <https://qudt.org/schema/qudt/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <https://schema.org/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX ssn: <http://www.w3.org/ns/ssn/>
PREFIX ssn-system: <https://www.w3.org/ns/ssn/systems/>
PREFIX unit: <https://qudt.org/vocab/unit/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/rdf.json>
    a
        foaf:Document ,
        schema:TextObject ;
    foaf:primaryTopic fst:0184ebd9-988b-7bb9-aab3-4241ffa5be84 ;
    schema:encodingFormat
        "application/json" ,
        "application/ld+json" ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/rdf.ttl>
    a
        foaf:Document ,
        schema:TextObject ;
    foaf:primaryTopic fst:0184ebd9-988b-7bb9-aab3-4241ffa5be84 ;
    schema:encodingFormat "text/turtle" ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/rdf.xml>
    a
        foaf:Document ,
        schema:TextObject ;
    foaf:primaryTopic fst:0184ebd9-988b-7bb9-aab3-4241ffa5be84 ;
    schema:encodingFormat "application/rdf+xml" ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/Bias/BiasUncertainty>
    a ssn:Property ;
    dcterms:conformsTo
        <https://doi.org/10.1007/978-3-030-78354-9> ,
        <https://dx.doi.org/10.2139/ssrn.4452038> ;
    rdfs:comment "<= typical: 0.005 %FS/°C, maximal: 0.01 %FS/°C (reference temperature according to datasheet most probably at 25°C)" ;
    rdfs:seeAlso
        <https://doi.org/10.1007/978-3-030-78354-9> ,
        <https://dx.doi.org/10.2139/ssrn.4452038> ;
    ssn:isPropertyOf <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/Bias> ;
    qudt:unit "bar/°C" ;
    schema:description "The bias uncertainty of the sensor of the linear transfer function of a sensor." ;
    schema:keywords
        "absolute" ,
        "relative_temperature_ambient" ;
    schema:name "bias uncertainty" ;
    schema:value 2.5e-04 ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/HysteresisUncertainty>
    a ssn:Property ;
    dcterms:conformsTo
        <https://doi.org/10.1007/978-3-030-78354-9> ,
        <https://dx.doi.org/10.2139/ssrn.4452038> ;
    rdfs:comment "typical: <= +- 0.2 %FS, maximal: <= +- 0.5 %FS" ;
    rdfs:seeAlso
        <https://doi.org/10.1007/978-3-030-78354-9> ,
        <https://dx.doi.org/10.2139/ssrn.4452038> ;
    ssn:isPropertyOf <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/SensorCapability> ;
    qudt:unit unit:BAR ;
    schema:description "The hysteresis uncertainty of the linear transfer function of a sensor." ;
    schema:keywords "absolute" ;
    schema:name "hysteresis uncertainty" ;
    schema:value 1e-02 ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/LinearityUncertainty>
    a ssn:Property ;
    dcterms:conformsTo
        <https://doi.org/10.1007/978-3-030-78354-9> ,
        <https://dx.doi.org/10.2139/ssrn.4452038> ;
    rdfs:comment "typical: <= +- 0.2 %FS, maximal: <= +- 0.5 %FS" ;
    rdfs:seeAlso
        <https://doi.org/10.1007/978-3-030-78354-9> ,
        <https://dx.doi.org/10.2139/ssrn.4452038> ;
    ssn:isPropertyOf <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/SensorCapability> ;
    qudt:unit unit:BAR ;
    schema:description "The linearity uncertainty of the linear transfer function of a sensor." ;
    schema:keywords "absolute" ;
    schema:name "linearity uncertainty" ;
    schema:value 1e-02 ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/MeasurementRange>
    a
        ssn:Property ,
        qudt:Quantity ,
        ssn-system:MeasurementRange ;
    ssn:isPropertyOf <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/SensorCapability> ;
    qudt:hasQuantityKind quantitykind:Pressure ;
    qudt:unit unit:BAR ;
    schema:maxValue 5e+00 ;
    schema:minValue 0e+00 ;
    schema:name "measurement range" ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/Sensitivity/SensitivityUncertainty>
    a ssn:Property ;
    dcterms:conformsTo
        <https://doi.org/10.1007/978-3-030-78354-9> ,
        <https://dx.doi.org/10.2139/ssrn.4452038> ;
    rdfs:comment "<= typical: 0.005 %MV/°C (measurement value), maximal: 0.02 %MV/°C (reference temperature according to datasheet most probably at 25°C)" ;
    rdfs:seeAlso
        <https://doi.org/10.1007/978-3-030-78354-9> ,
        <https://dx.doi.org/10.2139/ssrn.4452038> ;
    ssn:isPropertyOf <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/Sensitivity> ;
    qudt:unit "%MV/°C" ;
    schema:description "The sensitivity uncertainty of the linear transfer function of a sensor." ;
    schema:keywords
        "relative_mv" ,
        "relative_temperature_ambient" ;
    schema:name "sensitivity uncertainty" ;
    schema:value 5e-03 ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/SensorActuationRange>
    a
        ssn:Property ,
        qudt:Quantity ,
        ssn-system:ActuationRange ;
    ssn:isPropertyOf <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/SensorCapability> ;
    qudt:hasQuantityKind quantitykind:Voltage ;
    qudt:unit unit:V ;
    schema:maxValue 1e+01 ;
    schema:minValue 0e+00 ;
    schema:name "sensor output voltage range" ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/Bias>
    a
        ssn:Property ,
        ssn-system:SystemProperty ;
    rdfs:comment "offset" ;
    ssn:hasProperty <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/Bias/BiasUncertainty> ;
    ssn:isPropertyOf <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/SensorCapability> ;
    qudt:unit unit:BAR ;
    schema:name "bias" ;
    schema:value 0e+00 ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/Sensitivity>
    a
        ssn:Property ,
        ssn-system:Sensitivity ;
    rdfs:comment "gain" ;
    ssn:hasProperty <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/Sensitivity/SensitivityUncertainty> ;
    ssn:isPropertyOf <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/SensorCapability> ;
    qudt:unit "(bar)/(V)" ;
    schema:name "sensitivity" ;
    schema:value 5e-01 ;
.

fst:0184ebd9-988b-7bb9-aab3-4241ffa5be84
    a
        dcmitype:PhysicalObject ,
        sosa:Sensor ;
    dcterms:identifier
        "0184ebd9-988b-7bb9-aab3-4241ffa5be84" ,
        "fst-inv:D003" ;
    dcterms:modified "None" ;
    sosa:usedProcedure "Piezoresistiv" ;
    dbo:maintainedBy "Rexer" ;
    dbo:owner "FST" ;
    schema:documentation <docs/> ;
    schema:keywords
        "Druck" ,
        "Piezoresistiv" ;
    schema:location "Hydropulser Schrank" ;
    schema:manufacturer "Keller" ;
    schema:name "PA-23/8465-5" ;
    schema:serialNumber "74274" ;
    schema:subjectOf <docs/> ;
    ssn-system:hasSystemCapability <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/SensorCapability> ;
.

<https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/SensorCapability>
    a
        ssn:Property ,
        ssn-system:SystemCapability ;
    rdfs:comment "sensor capabilities not regarding any conditions at this time" ;
    ssn:hasProperty
        <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/Bias> ,
        <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/HysteresisUncertainty> ,
        <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/LinearityUncertainty> ,
        <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/MeasurementRange> ,
        <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/Sensitivity> ,
        <https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aab3-4241ffa5be84/SensorActuationRange> ;
    schema:name "sensor capabilities" ;
.

