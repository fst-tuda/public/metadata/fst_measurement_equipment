## Sensor 8210-300, 210096

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b04a-915584cbd834`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b04a-915584cbd834)
### UUID: `0184ebd9-988b-7bb9-b04a-915584cbd834`
### identifier: `fst-inv:D25`

</div>

## Keywords: DMS, Druck

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Burster |
| name: | 8210-300, 210096 |
| serial number: | 43484 |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b04a-915584cbd834/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

