## Sensor PBMN

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b91a-a00d6c00b9ba`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b91a-a00d6c00b9ba)
### UUID: `0184ebd9-988b-7bb9-b91a-a00d6c00b9ba`
### identifier: `fst-inv:D58`

</div>

## Keywords: DMS, Druck, relativ

## General Info

| property | value |
|-:|:-|
| comment: | PBMN-2.5.B33.R.A2.44.06.4.1.0.0.0.0.000 |
| manufacturer: | Baumer GmbH |
| name: | PBMN |
| serial number: | 2.17.101924493.1 |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Schänzle |
| last known location: | Hydraulikpumpenprüfstand |
| last modification: | 2017-09-01T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b91a-a00d6c00b9ba/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

