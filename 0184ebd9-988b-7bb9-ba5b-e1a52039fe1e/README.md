## Sensor PD-33X/80920

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-ba5b-e1a52039fe1e`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-ba5b-e1a52039fe1e)
### UUID: `0184ebd9-988b-7bb9-ba5b-e1a52039fe1e`
### identifier: `fst-inv:D63`

</div>

## Keywords: Differenzdruck, Druck, relativ

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PD-33X/80920 |
| serial number: | 509639 |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Normprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-ba5b-e1a52039fe1e/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

