## Sensor 661.20F-02

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9617-8e614e5f0e4f`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9617-8e614e5f0e4f)
### UUID: `0184ebd9-988c-7bbb-9617-8e614e5f0e4f`
### identifier: `fst-inv:K03`

</div>

## Keywords: DMS, Kraft

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | MTS |
| name: | 661.20F-02 |
| serial number: | 356069 |
| used procedure: | DMS |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-9617-8e614e5f0e4f/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

