## Sensor PAA-33X/80794

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-83a5-01cec15c9820`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-83a5-01cec15c9820)
### UUID: `0184ebd9-988b-7bba-83a5-01cec15c9820`
### identifier: `fst-inv:D098`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-33X/80794 |
| serial number: | 431003 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Prüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-83a5-01cec15c9820/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

