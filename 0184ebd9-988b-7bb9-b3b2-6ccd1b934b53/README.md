## Sensor PU50

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b3b2-6ccd1b934b53`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b3b2-6ccd1b934b53)
### UUID: `0184ebd9-988b-7bb9-b3b2-6ccd1b934b53`
### identifier: `fst-inv:D37`

</div>

## Keywords: Differenzdruck, Druck

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Halstrup Multur |
| name: | PU50 |
| serial number: | None |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Grenzschichtprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b3b2-6ccd1b934b53/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

