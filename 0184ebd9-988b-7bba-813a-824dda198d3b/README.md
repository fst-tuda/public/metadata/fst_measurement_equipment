## Sensor PD-23/8666- -10/+10

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bba-813a-824dda198d3b`](https://w3id.org/fst/resource/0184ebd9-988b-7bba-813a-824dda198d3b)
### UUID: `0184ebd9-988b-7bba-813a-824dda198d3b`
### identifier: `fst-inv:D088`

</div>

## Keywords: Differenzdruck, Druck, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PD-23/8666- -10/+10 |
| serial number: | 79699 |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bba-813a-824dda198d3b/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

