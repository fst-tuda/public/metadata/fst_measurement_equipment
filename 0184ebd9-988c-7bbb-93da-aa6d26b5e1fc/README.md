## Sensor LWG225

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-93da-aa6d26b5e1fc`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-93da-aa6d26b5e1fc)
### UUID: `0184ebd9-988c-7bbb-93da-aa6d26b5e1fc`
### identifier: `fst-inv:W12`

</div>

## Keywords: Induktiv, Weg

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Novotechnik |
| name: | LWG225 |
| serial number: | None |
| used procedure: | Induktiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-93da-aa6d26b5e1fc/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

