## Sensor Sensor-0185217b-73cb-750b-abbf-a76c31616b15

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0185217b-73cb-750b-abbf-a76c31616b15`](https://w3id.org/fst/resource/0185217b-73cb-750b-abbf-a76c31616b15)
### UUID: `0185217b-73cb-750b-abbf-a76c31616b15`
### identifier: `fst-inv:W19`

</div>

## Keywords: Weg, induktiv

## General Info

| property | value |
|-:|:-|
| comment: | Im Hydropulser fest verbaut |
| manufacturer: | MTS |
| name: | Sensor-0185217b-73cb-750b-abbf-a76c31616b15 |
| serial number: | None |
| used procedure: | induktiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0185217b-73cb-750b-abbf-a76c31616b15/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

