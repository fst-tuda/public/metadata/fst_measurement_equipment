## Sensor PA-25TAB/80087

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-afa9-4ad47c116bca`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-afa9-4ad47c116bca)
### UUID: `0184ebd9-988b-7bb9-afa9-4ad47c116bca`
### identifier: `fst-inv:D22`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PA-25TAB/80087 |
| serial number: | 60035 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Lager Messtechnik |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-afa9-4ad47c116bca/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

