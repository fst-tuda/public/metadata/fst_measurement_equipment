## Sensor PD-23 / 8666

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-ba27-8fb379f8824f`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-ba27-8fb379f8824f)
### UUID: `0184ebd9-988b-7bb9-ba27-8fb379f8824f`
### identifier: `fst-inv:D62`

</div>

## Keywords: Differenzdruck, Druck, relativ

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PD-23 / 8666 |
| serial number: | 907079 |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Kuhr |
| last known location: | Gleitlagerprüfstand |
| last modification: | 2018-09-22T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-ba27-8fb379f8824f/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

