## Sensor PA-23/8465-300

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-bddb-9e34ed0ac9d4`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-bddb-9e34ed0ac9d4)
### UUID: `0184ebd9-988b-7bb9-bddb-9e34ed0ac9d4`
### identifier: `fst-inv:D76`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PA-23/8465-300 |
| serial number: | 89574 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-bddb-9e34ed0ac9d4/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

