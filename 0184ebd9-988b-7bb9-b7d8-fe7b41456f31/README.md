## Sensor PSI 9016

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b7d8-fe7b41456f31`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b7d8-fe7b41456f31)
### UUID: `0184ebd9-988b-7bb9-b7d8-fe7b41456f31`
### identifier: `fst-inv:D53`

</div>

## Keywords: Differenzdruck, Druck, relativ

## General Info

| property | value |
|-:|:-|
| comment: | 8 Kanäle, zusammen mit D52 |
| manufacturer: | Pressure Systems, Inc. |
| name: | PSI 9016 |
| serial number: | S/N 1182 |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Saul |
| last known location: | Ventilatorprüfstände |
| last modification: | 2014-08-21T00:00:00 |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b7d8-fe7b41456f31/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

