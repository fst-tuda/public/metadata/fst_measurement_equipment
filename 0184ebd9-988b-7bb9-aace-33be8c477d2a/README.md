## Sensor PAA-23/5bar/8465

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aace-33be8c477d2a`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aace-33be8c477d2a)
### UUID: `0184ebd9-988b-7bb9-aace-33be8c477d2a`
### identifier: `fst-inv:D004`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-23/5bar/8465 |
| serial number: | 81248 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-aace-33be8c477d2a/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

