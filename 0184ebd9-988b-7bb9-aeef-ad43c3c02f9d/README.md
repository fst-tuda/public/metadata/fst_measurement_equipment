## Sensor BHL-4201-00-03

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aeef-ad43c3c02f9d`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aeef-ad43c3c02f9d)
### UUID: `0184ebd9-988b-7bb9-aeef-ad43c3c02f9d`
### identifier: `fst-inv:D19`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | TransInstruments |
| name: | BHL-4201-00-03 |
| serial number: | L423874 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-aeef-ad43c3c02f9d/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

