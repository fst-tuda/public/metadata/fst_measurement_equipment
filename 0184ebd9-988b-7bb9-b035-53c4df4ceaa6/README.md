## Sensor PL 209, 423-209-000-041

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b035-53c4df4ceaa6`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b035-53c4df4ceaa6)
### UUID: `0184ebd9-988b-7bb9-b035-53c4df4ceaa6`
### identifier: `fst-inv:D24`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | vibro-meter |
| name: | PL 209, 423-209-000-041 |
| serial number: | 250 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b035-53c4df4ceaa6/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

