## Sensor PAA-33X / 80794

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b96f-f0c1de70b6b3`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b96f-f0c1de70b6b3)
### UUID: `0184ebd9-988b-7bb9-b96f-f0c1de70b6b3`
### identifier: `fst-inv:D59`

</div>

## Keywords: Druck, Piezoresistiv, absolut

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PAA-33X / 80794 |
| serial number: | 147667 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Schänzle |
| last known location: | Hydraulikpumpenprüfstand |
| last modification: | n.a. |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b96f-f0c1de70b6b3/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

