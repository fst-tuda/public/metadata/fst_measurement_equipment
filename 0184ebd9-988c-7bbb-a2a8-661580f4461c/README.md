## Sensor ETS 4146-B-006-000

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-a2a8-661580f4461c`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-a2a8-661580f4461c)
### UUID: `0184ebd9-988c-7bbb-a2a8-661580f4461c`
### identifier: `fst-inv:T13`

</div>

## Keywords: Pt100, Temperatur

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Hydac |
| name: | ETS 4146-B-006-000 |
| serial number: | 617P014983 |
| used procedure: | Pt100 |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-a2a8-661580f4461c/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

