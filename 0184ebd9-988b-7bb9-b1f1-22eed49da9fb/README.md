## Sensor n.e.

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b1f1-22eed49da9fb`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b1f1-22eed49da9fb)
### UUID: `0184ebd9-988b-7bb9-b1f1-22eed49da9fb`
### identifier: `fst-inv:D30`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | n.e. |
| serial number: | 73152 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b1f1-22eed49da9fb/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

