## Sensor TYP 102

<div align="right">

### IRI: [`https://w3id.org/fst/resource/018a058b-c08d-79ba-8e37-2a4c2bec56b5`](https://w3id.org/fst/resource/018a058b-c08d-79ba-8e37-2a4c2bec56b5)
### UUID: `018a058b-c08d-79ba-8e37-2a4c2bec56b5`
### identifier: `fst-inv:T22`

</div>

## Keywords: Messumformer/Thermoelement Typ K, Temperatur

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | LKM |
| name: | TYP 102 |
| serial number: | 184416 Kennnummer |
| used procedure: | Messumformer/Thermoelement Typ K |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/018a058b-c08d-79ba-8e37-2a4c2bec56b5/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

