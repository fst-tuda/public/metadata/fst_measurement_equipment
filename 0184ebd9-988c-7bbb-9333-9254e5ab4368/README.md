## Sensor RF605

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9333-9254e5ab4368`](https://w3id.org/fst/resource/0184ebd9-988c-7bbb-9333-9254e5ab4368)
### UUID: `0184ebd9-988c-7bbb-9333-9254e5ab4368`
### identifier: `fst-inv:W09`

</div>

## Keywords: Optisch, Weg

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | RIFTEK |
| name: | RF605 |
| serial number: | 25682 |
| used procedure: | Optisch |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988c-7bbb-9333-9254e5ab4368/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

