## Sensor A08 XHT

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b36b-eac46d3cb33a`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b36b-eac46d3cb33a)
### UUID: `0184ebd9-988b-7bb9-b36b-eac46d3cb33a`
### identifier: `fst-inv:D36`

</div>

## Keywords: Druck

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | STW |
| name: | A08 XHT |
| serial number: | 03.003196.3 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Kuhr |
| last known location: | Büro 471 |
| last modification: | None |
|-|-|
| related resources: | WIKA Manometer 0 - 16 bar |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b36b-eac46d3cb33a/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

