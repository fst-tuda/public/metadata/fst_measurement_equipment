## Sensor Sensor-0184ebd9-988b-7bb9-b405-5cf34d36262e

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b405-5cf34d36262e`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b405-5cf34d36262e)
### UUID: `0184ebd9-988b-7bb9-b405-5cf34d36262e`
### identifier: `fst-inv:D39`

</div>

## Keywords: Druck

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Halstrup Multur |
| name: | Sensor-0184ebd9-988b-7bb9-b405-5cf34d36262e |
| serial number: | None |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Grenzschichtprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b405-5cf34d36262e/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

