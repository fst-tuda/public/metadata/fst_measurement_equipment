## Sensor O-10 (C) 

<div align="right">

### IRI: [`https://w3id.org/fst/resource/018a058b-c08f-7c04-a5ac-eb91780ae4aa`](https://w3id.org/fst/resource/018a058b-c08f-7c04-a5ac-eb91780ae4aa)
### UUID: `018a058b-c08f-7c04-a5ac-eb91780ae4aa`
### identifier: `fst-inv:D130`

</div>

## Keywords: Druck, relativ

## General Info

| property | value |
|-:|:-|
| comment: | 
neu |
| manufacturer: | WIKA |
| name: | O-10 (C)  |
| serial number: | 110J2CXA |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [file:///home/sebastian/Desktop/hydropulser-database-scripts/_generated/pID_directories/018a058b-c08f-7c04-a5ac-eb91780ae4aa/docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/018a058b-c08f-7c04-a5ac-eb91780ae4aa/file:///home/sebastian/Desktop/hydropulser-database-scripts/_generated/pID_directories/018a058b-c08f-7c04-a5ac-eb91780ae4aa/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

