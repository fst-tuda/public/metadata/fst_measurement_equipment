## Sensor PR 15, 3403-18-71.33

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b14d-64bd3544d6b9`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b14d-64bd3544d6b9)
### UUID: `0184ebd9-988b-7bb9-b14d-64bd3544d6b9`
### identifier: `fst-inv:D28`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Hydrotechnik GmbH |
| name: | PR 15, 3403-18-71.33 |
| serial number: | 1191 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b14d-64bd3544d6b9/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

