## Sensor PD-23/8666-0.2

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aa48-34144ed09467`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-aa48-34144ed09467)
### UUID: `0184ebd9-988b-7bb9-aa48-34144ed09467`
### identifier: `fst-inv:D002`

</div>

## Keywords: Druck, Piezoresistiv

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Keller |
| name: | PD-23/8666-0.2 |
| serial number: | 80876 |
| used procedure: | Piezoresistiv |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | Hydropulser Schrank |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-aa48-34144ed09467/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

