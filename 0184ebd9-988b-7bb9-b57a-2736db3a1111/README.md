## Sensor PU25

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b57a-2736db3a1111`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b57a-2736db3a1111)
### UUID: `0184ebd9-988b-7bb9-b57a-2736db3a1111`
### identifier: `fst-inv:D44`

</div>

## Keywords: Druck

## General Info

| property | value |
|-:|:-|
| comment: | ohne Netzstecker |
| manufacturer: | Halstrup Multur |
| name: | PU25 |
| serial number: | 291087020064 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Grenzschichtprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b57a-2736db3a1111/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

