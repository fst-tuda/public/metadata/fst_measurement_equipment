## Sensor LBA025B

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b48b-62ba0532f2bc`](https://w3id.org/fst/resource/0184ebd9-988b-7bb9-b48b-62ba0532f2bc)
### UUID: `0184ebd9-988b-7bb9-b48b-62ba0532f2bc`
### identifier: `fst-inv:D41`

</div>

## Keywords: Differenzdruck, Druck

## General Info

| property | value |
|-:|:-|
| comment: | None |
| manufacturer: | Sensortechnics |
| name: | LBA025B |
| serial number: | None |
| used procedure: | Differenzdruck |
|-|-|
| owner: | FST |
| maintainer: | Hermann |
| last known location: | Grenzschichtprüfstand |
| last modification: | None |
|-|-|
| related resources: | None |
| documentation: | [docs/](https://git.rwth-aachen.de/fst-tuda/public/metadata/fst_measurement_equipment/-/blob/main/0184ebd9-988b-7bb9-b48b-62ba0532f2bc/docs/) |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, http://www.w3.org/ns/sosa/Sensor |

